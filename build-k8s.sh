#!/bin/bash

COUNT="0";
NODE_NUMBER="1";
CPU="2";
DISK="15G";
MEMORY="2G";
CIFILE="init-k8s.yaml";

for (( COUNT; COUNT <= NODE_NUMBER ; COUNT++ )); do
    if [[ COUNT -eq "0" ]]; then
        NAME="k8s-master";
    else
        NAME="k8s-node$COUNT";
    fi
    
    multipass launch --cpus "$CPU" \
        --mem "$MEMORY" \
        --disk "$DISK" \
        --name "$NAME" \
        --cloud-init "$CIFILE"
done