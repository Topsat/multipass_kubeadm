#cloud-config

packages:
  - apt-transport-https
  - ca-certificates
  - curl
  - gnupg-agent
  - software-properties-common
  - bash-completion

users:
  - name: "ubuntu"
    sudo: ['ALL=(ALL) NOPASSWD:ALL']
    groups: [ sudo ]
    homedir: "/home/ubuntu"
    shell: "/bin/bash"

# Enable ipv4 forwarding, required on CIS hardened machines
write_files:
  - path: /etc/sysctl.d/enabled_ipv4_forwarding.conf
    content: |
      net.ipv4.conf.all.forwarding=1
  - path: /etc/docker/daemon.json
    content: |
      {
        "exec-opts": ["native.cgroupdriver=systemd"]
      }
  - path: /etc/sysctl.d/k8s.conf
    content: |
      net.bridge.bridge-nf-call-ip6tables = 1
      net.bridge.bridge-nf-call-iptables = 1
  - path: /tmp/kubeadm-init.yaml
    content: |
      apiVersion: kubeadm.k8s.io/v1beta2
      kubernetesVersion: 1.24.10
      kind: ClusterConfiguration
      networking:
        podSubnet: 192.168.0.0/16
      controlPlaneEndpoint: "k8s-master:6443"
  - path: /tmp/init-script.sh
    owner: ubuntu:ubuntu
    permissions: "0544"
    content: |
      #!/bin/bash
      mkdir -p ~/.kube
      sudo cp -i /etc/kubernetes/admin.conf ~/.kube/config
      sudo chown $(id -u):$(id -g) ~/.kube/config
      echo 'source /usr/share/bash-completion/bash_completion' >> ~/.bashrc
      echo 'source <(kubectl completion bash)' >> ~/.bashrc
      echo 'alias k=kubectl' >> ~/.bashrc
      echo 'complete -F __start_kubectl k' >> ~/.bashrc
      echo "alias etcdctl='kubectl exec -it -n kube-system etcd-k8s-master -- etcdctl --endpoints https://localhost:2379 --cert=/etc/kubernetes/pki/etcd/server.crt --key=/etc/kubernetes/pki/etcd/server.key --cacert=/etc/kubernetes/pki/etcd/ca.crt' >> ~/.bashrc"
      kubectl apply -f https://docs.projectcalico.org/manifests/calico.yaml
      # kubectl apply -f "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 | tr -d '\n')"

# create the docker group
groups:
  - docker

# Install Docker, for production, consider pinning to stable versions
runcmd:
  - curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
  - curl -fsSL https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
  - add-apt-repository "deb [arch=$(dpkg --print-architecture)] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
  - add-apt-repository "deb https://apt.kubernetes.io/ kubernetes-xenial main"
  - apt-get update -y
  - apt-get install -y docker-ce docker-ce-cli containerd.io
  - apt-get install -y kubelet=1.24.10-00 kubeadm=1.24.10-00 kubectl=1.24.10-00
  - apt-mark hold kubelet kubeadm kubectl
  - containerd config default > /etc/containerd/config.toml
  - sed -i 's/SystemdCgroup = false/SystemdCgroup = true/' /etc/containerd/config.toml
  - systemctl start docker
  - systemctl enable docker
  - systemctl restart containerd
  - systemctl enable containerd
  - sysctl --system
  - kubeadm init --config /tmp/kubeadm-init.yaml --ignore-preflight-errors=NumCPU --upload-certs | tee /var/log/kubeadm-init.log
  - su - ubuntu -c "/bin/bash /tmp/init-script.sh"

# Add default auto created user to docker group
system_info:
  default_user:
    groups: [docker]