# multipass k8s v0.0.1

This script is useful for creating & deleting k8s instances through multipass.

launch_k8s.sh is the script who permit to launch instances

superdelete-k8s.sh delete & purge all instances one by one.

cloud-template.yaml is the template configuration file needed to install docker, kubeadm, kubelet & kubectl on each instances.

# multipass k8s v0.0.2
renaming launch_k8s.sh to build-k8s.sh
add a cloud-init file named, init-k8s.yaml
