#!/bin/bash

NAME="k8s-master"
COUNT="0"
NODE_NUMBER="0" #// SET VALUE : "TOTAL MACHINE NUMBER-1"

if [[ COUNT -eq 0 ]]; then
    multipass stop "$NAME";
    multipass delete "$NAME";
fi

for (( COUNT; COUNT <= NODE_NUMBER; COUNT++ )); do
    NAME="k8s-node$COUNT";
    if [[ COUNT -ge 1 ]]; then
        multipass stop "$NAME";
        multipass delete "$NAME";
    fi
    if [[ COUNT -eq NODE_NUMBER ]]; then
        multipass purge;
        echo "multipass has been super purged !"
    fi
done
